/**************************************************************
 * Blynk is a platform with iOS and Android apps to control
 * Arduino, Raspberry Pi and the likes over the Internet.
 * You can easily build graphic interfaces for all your
 * projects by simply dragging and dropping widgets.
 *
 *   Downloads, docs, tutorials: http://www.blynk.cc
 *   Blynk community:            http://community.blynk.cc
 *   Social groups:              http://www.fb.com/blynkapp
 *                               http://twitter.com/blynk_app
 *
 * Blynk library is licensed under MIT license
 * This example code is in public domain.
 *
 **************************************************************
 * This example runs directly on ESP8266 chip.
 *
 * You need to install this for ESP8266 development:
 *   https://github.com/esp8266/Arduino
 *
 * Change WiFi ssid, pass, and Blynk auth token to run :)
 *
 **************************************************************/

#define BLYNK_PRINT Serial    // Comment this out to disable prints and save space
#define BLYNK_EXPERIMENTAL

#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

#include <EEPROM.h>
#include <ESP8266mDNS.h>
#include <WifiManager.h>

#include <SimpleTimer.h>


WiFiManager wifi(0);

SimpleTimer timer;
int timerId;

//WidgetLED led1(1);

boolean started = false;
long wateringStart = 0;
long wateringLength = 0;

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "cb5c7e5377aa4c69b8746b48fdea58b8";

void setup()
{

  Serial.begin(115200);
  wifi.autoConnect("Blynk");
  String ssid = wifi.getSSID();
  String pass = wifi.getPassword();

  Blynk.begin(auth, ssid.c_str(), pass.c_str());
  //Blynk.begin(auth, ssid.c_str(), "");

  pinMode(5, OUTPUT);
  digitalWrite(5, LOW);

  timer.setInterval(1000, heartBeat);

  Serial.println("done setup");
}


void loop()
{
  Blynk.run();
  timer.run();
  if (!started && Blynk.connected()) {
    //intial setup after connected
    started = true;
    for (int i = 0; i < 7; i++) {
      Blynk.virtualWrite(1, i % 2);
      delay(200);
    }
  }
}

void heartBeat() {
  if ( wateringLength != 0 ) {
    if ((wateringStart + wateringLength - millis()) < 1000) {
      //wateringLength = 0;
      Serial.println("done watering");
      digitalWrite(5, LOW);
      wateringStart = 0;
      int i = digitalRead(5);
      Blynk.virtualWrite(1, i);
      Blynk.push_notification("watering done");
    }
  }
}

// This takes the value of virtual pin 0 in blynk program...  if it is 1 turns on, else off...
BLYNK_WRITE(0) {
  int a = param.asInt();
  if (a == 0) {
    digitalWrite(5, LOW);

    wateringStart = 0;
  } else {
    digitalWrite(5, HIGH);
    if (wateringStart == 0) {
      wateringStart = millis();
      //Blynk.virtualRead(6);
    }
  }
  Blynk.virtualWrite(1, a);
  Serial.println("write");
}

//// This publishes the int power back to the blynk program...

BLYNK_READ(2) {
  //Serial.println("read");

  long uptime = millis() / 1000 / 60;
  Blynk.virtualWrite(2, uptime);
}

BLYNK_WRITE(3) {
  int a = param.asInt();
  if (a != 0) {
    Serial.print("refresh");
    int i = digitalRead(5);
    Blynk.virtualWrite(1, i);
  }
}

BLYNK_READ(4) {
  //Serial.println("read watering interval");

  long watering = 0;
  if ( wateringStart != 0 ) {
    watering = (millis() - wateringStart) / 1000 ;
  }

  Blynk.virtualWrite(4, watering);
}

//read timer left
BLYNK_READ(5) {
  //Serial.println("read");

  long remaining = 0;
  if ( wateringLength != 0 && wateringStart != 0) {
    remaining = (wateringStart + wateringLength - millis()) / 1000 ;
  } else {
    remaining = wateringLength / 1000;
  }

  Blynk.virtualWrite(5, remaining);
}


BLYNK_WRITE(6) {
  //Serial.println("write watering length");
  long a = param.asLong();
  //Serial.print("write legth");
  wateringLength = a * 1000;
}

//timer virtual pin
BLYNK_WRITE(10) {
  int a = param.asInt();
  if (a == 0) {
    digitalWrite(5, LOW);

    wateringStart = 0;
  } else {
    digitalWrite(5, HIGH);
    if (wateringStart == 0) {
      wateringStart = millis();
    }
  }
  Blynk.virtualWrite(1, a);
  Serial.println("write");
}

BLYNK_WRITE(20) {
  int a = param.asInt();
  if (a == 0) {
    Serial.println("reset");
    ESP.reset();
  }
}

